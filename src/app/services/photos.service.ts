import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {Photos} from '../interfaces/photos';
import {FlickrImages} from '../interfaces/flickr-images';

@Injectable({
  providedIn: 'root'
})
export class PhotosService {
  async = true;
  crossDomain = true;

  apiKey = '1575439375f8aeb55ab0a651d0583de6';
  apiSig = '77ec8a88d96301299a02fcfec820572d';

  constructor(private http: HttpClient) { }

  getImagesFromFlickr(tags): Observable<FlickrImages> {
    console.log('CALLING : https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=' +
      this.apiKey + '&tags=' + tags + '&safe_search=1&format=json&nojsoncallback=1');
    return this.http.get<FlickrImages>('https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=' +
      this.apiKey + '&tags=' + tags + '&safe_search=1&format=json&nojsoncallback=1');
  }
}
