import {Photos} from './photos';

export interface FlickrImages {
  photos: Photos;
}
