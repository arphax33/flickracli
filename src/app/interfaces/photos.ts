import {Photo} from './photo';

export interface Photos {
  page: number;
  pages: number;
  perpages: number;
  total: string;
  photo: Photo[];
}
