import {Component, EventEmitter, Output, ViewChild} from '@angular/core';
import {ImagesboxComponent} from './components/imagesbox/imagesbox.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // @ts-ignore
  @ViewChild(ImagesboxComponent) imagesBox: ImagesboxComponent;
  title = 'FlickrSearchACLI';

  changeImg(uri: string) {
    this.imagesBox.getImagesByTags(uri);
  }
}
