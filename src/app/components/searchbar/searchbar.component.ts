import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.css']
})
export class SearchbarComponent implements OnInit {
  @Output() uri = new EventEmitter();
  tags: string;

  constructor() { }

  ngOnInit() {
  }

  search() {
    console.log('SEARCHING FOR :' + this.tags);
    this.uri.emit(this.tags);
  }

}
