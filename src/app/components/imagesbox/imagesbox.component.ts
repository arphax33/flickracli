import { Component, OnInit } from '@angular/core';
import {PhotosService} from '../../services/photos.service';
import {HttpErrorResponse} from '@angular/common/http';
import {Photo} from '../../interfaces/photo';
import {Photos} from '../../interfaces/photos';

@Component({
  selector: 'app-imagesbox',
  templateUrl: './imagesbox.component.html',
  styleUrls: ['./imagesbox.component.css']
})
export class ImagesboxComponent implements OnInit {

  photos: Photos;

  constructor(private photosService: PhotosService) { }

  ngOnInit() {
  }

  getImagesByTags(tags) {
    this.photosService.getImagesFromFlickr(tags).subscribe(
      data => {
        console.log('RESULT : ' + data.photos);
        this.photos = data.photos;
      },
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          console.log('Erreur client');
        } else {
          console.log('Erreur serveur');
        }
      });
  }
}
